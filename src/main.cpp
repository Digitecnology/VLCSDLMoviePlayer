#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_version.h>
#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_mutex.h>
#include <SDL2/SDL_timer.h>
#include <vlc/vlc.h>
#include <stdint.h>
#include <unistd.h>
#include "vlciso369.h"

// SDL-related variables
SDL_Window *window;
SDL_Renderer *renderer;
SDL_Event event;
SDL_Texture *texture;
SDL_AudioDeviceID audio_device;
SDL_AudioSpec desired_spec;
SDL_AudioSpec obtained_spec;

// VLC-related variables
libvlc_instance_t *instance = NULL;
libvlc_media_t *media = NULL;
libvlc_media_player_t *player = NULL;

// General variables
bool v_update = false;
void *v_buffer = NULL;
void *v_pixels = NULL;
int v_pitch = 0;
const char *filename = "";
const int fps = 30;
int timer = 0;
bool has_tracks = false;

static void AudioPlay(void *data, const void *samples, unsigned count,
					  int64_t pts)
{
	SDL_QueueAudio(audio_device, samples, count * 2 * 1);
}

static void *VideoLock(void *opaque, void **planes)
{
	*planes = v_buffer;

	return NULL;
}

static void VideoUnlock(void *opaque, void *picture, void *const *planes) {}

static void VideoDisplay(void *opaque, void *picture)
{
	v_update = true;
}

static void EventPlayed(const struct libvlc_event_t *event, void *opaque)
{
	libvlc_media_player_set_pause((libvlc_media_player_t *)opaque, 1);
	has_tracks = true;
}

int Hello()
{
	return 37;
}

int main(int argc, char **argv)
{
	// SDL part
	//=====================================================

	Hello();

	// Initialize
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER) == 0)
		printf("SDL load was sucessfull ;)\n");
	else
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to load SDL: %s",
					 SDL_GetError());
		return -1;
	}

	// Get and show version
	SDL_version version;

	SDL_GetVersion(&version);

	printf("Using SDL %i.%i.%i\n", version.major, version.minor, version.patch);

	// Open audio device
	SDL_memset(&desired_spec, 0, sizeof(desired_spec));

	desired_spec.callback = NULL;
	desired_spec.channels = 1;
	desired_spec.format = AUDIO_S16LSB;
	desired_spec.freq = 44100;
	desired_spec.userdata = NULL;
	desired_spec.samples = 4096;

	audio_device = SDL_OpenAudioDevice(SDL_GetAudioDeviceName(0, 0), 0,
									   &desired_spec, &obtained_spec, 0);

	if (audio_device < 0)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
					 "Couldn't create audio device: %s", SDL_GetError());
		return -1;
	}

	// Create and open renderer and window
	if (SDL_CreateWindowAndRenderer(800, 600, 0, &window, &renderer))
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
					 "Couldn't create window and renderer: %s", SDL_GetError());
		return -1;
	}

	SDL_SetWindowTitle(window, "VLC Media Player using SDL");

	// Play audio device
	SDL_PauseAudioDevice(audio_device, 0);

	// Create texture
	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888,
								SDL_TEXTUREACCESS_STREAMING, 800, 600);

	if (texture == NULL)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to create texture: %s",
					 SDL_GetError());
		return -1;
	}

	// Allocate video buffer
	v_buffer = aligned_alloc(32, 800 * 600 * 4);

	if (v_buffer == NULL)
	{
		printf("Error allocating video buffer.\n");
		return -1;
	}

	// VLC part
	//=====================================================

	// Get/create system and VLC arguments
	const char *vlc_argv[] =
	{
		"--no-xlib",
		"--vout=dummy",
		"--aout=dummy",
	};

	int vlc_argc = sizeof(vlc_argv) / sizeof(*vlc_argv);

	if (argc > 1)
		filename = argv[1];
	else
	{
		printf("Usage: ./VLCSDLMoviePlayer <filename>\n");
		return -1;
	}

	printf("Filename used to play: %s\n", filename);

	// Create instance
	instance = libvlc_new(vlc_argc, vlc_argv);

	if (instance == NULL)
	{
		printf("Could not init VLC :(\n");
		return -1;
	}
	else
		printf("Initialized VLC ;)\n");

	printf("Using VLC %s", libvlc_get_version());
	printf("Using VLC environment VLC_PLUGIN_PATH=%s\n",
		   getenv("VLC_PLUGIN_PATH"));

	// Create media
	media = libvlc_media_new_path(instance, filename);

	if (media == NULL)
	{
		printf("Could not open video file :(\n");
		return -1;
	}

	// Create player
	player = libvlc_media_player_new_from_media(media);

	if (player == NULL)
	{
		printf("Could not play video file :(\n");
		return -1;
	}

	printf("Language code: %s\n", FVlcISO639::GetLangDescription("und"));

	libvlc_media_release(media);

	if (libvlc_media_player_get_media(player) == NULL)
		printf("Warning, media is NULL\n");

	// Set the format of the player
	libvlc_video_set_format(player, "RV32", 800, 600, 800 * 4);
	libvlc_audio_set_format(player, "S16L", 44100, 1);

	// Set the callbacks for audio and video
	libvlc_video_set_callbacks(player, &VideoLock, &VideoUnlock, &VideoDisplay,
							   NULL);
	libvlc_audio_set_callbacks(player, &AudioPlay, NULL, NULL, NULL, NULL, NULL);

	// Set all black pixels for texture
	SDL_LockTexture(texture, NULL, &v_pixels, &v_pitch);
	memset(v_pixels, 0x00, 800 * 600 * 4);
	SDL_UnlockTexture(texture);

	// HACK: Attach a playing event to get real track data
	libvlc_media_parse(libvlc_media_player_get_media(player));
	//libvlc_event_attach(libvlc_media_player_event_manager(player),
	//					libvlc_MediaPlayerPlaying, &EventPlayed, player);
	libvlc_media_player_play(player);
	while(libvlc_media_player_get_state(player) != libvlc_Playing);
	libvlc_media_player_set_pause(player, 1);
	while(libvlc_media_player_get_state(player) != libvlc_Paused);

	// Get media information
	printf("Duration: %i\n",
		   libvlc_media_get_duration(libvlc_media_player_get_media(player)));
	printf("Parsed: %i\n",
		   libvlc_media_is_parsed(libvlc_media_player_get_media(player)));
	printf("Will Play: %i\n", libvlc_media_player_will_play(player));
	printf("Rate: %i\n", libvlc_media_player_get_rate(player));
	printf("State: %i\n", libvlc_media_player_get_state(player));

	// Get tracks information
	printf("Track count: %i\n", libvlc_video_get_track_count(player));

	libvlc_media_track_t **m_tracks;
	unsigned int n_tracks =
		libvlc_media_tracks_get(libvlc_media_player_get_media(player), &m_tracks);

	printf("Total number of tracks: %u\n", n_tracks);

	for (int i = 0; i < n_tracks; i++)
	{
		printf("----------------\n");
		printf("Track %i:\n", i);
		printf("----------------\n");

		switch (m_tracks[i]->i_type)
		{
		case libvlc_track_audio:
			printf("Type: Audio\n");
			printf("ID: %i\n", m_tracks[i]->i_id);
			printf("Description: %s\n", m_tracks[i]->psz_description);
			printf("Language: %s\n", m_tracks[i]->psz_language);
			printf("Bitrate: %i\n", m_tracks[i]->i_bitrate);
			printf("Audio Channels: %i\n", m_tracks[i]->audio->i_channels);
			printf("Audio Rate: %i\n", m_tracks[i]->audio->i_rate);
			break;
		case libvlc_track_video:
			printf("Type: Video\n");
			printf("ID: %i\n", m_tracks[i]->i_id);
			printf("Description: %s\n", m_tracks[i]->psz_description);
			printf("Language: %s\n", m_tracks[i]->psz_language);
			printf("Bitrate: %i\n", m_tracks[i]->i_bitrate);
			printf("Video Width: %i\n", m_tracks[i]->video->i_width);
			printf("Video Height: %i\n", m_tracks[i]->video->i_height);
			break;
		case libvlc_track_text:
			printf("Type: Text\n");
			printf("ID: %i\n", m_tracks[i]->i_id);
			printf("Description: %s\n", m_tracks[i]->psz_description);
			printf("Language: %s\n", m_tracks[i]->psz_language);
			printf("Bitrate: %i\n", m_tracks[i]->i_bitrate);
			printf("Encoding: %i\n", m_tracks[i]->subtitle->psz_encoding);
			break;
		}
	}

	libvlc_track_description_t *v_tracks;
	v_tracks = libvlc_video_get_track_description(player);

	while (v_tracks != NULL)
	{
		printf("----------------\n");
		printf("VTrack\n");
		printf("----------------\n");
		printf("ID: %i\n", v_tracks->i_id);
		printf("Name: %s\n", v_tracks->psz_name);

		v_tracks = v_tracks->p_next;
	}

	libvlc_track_description_t *a_tracks;
	a_tracks = libvlc_audio_get_track_description(player);

	while (a_tracks != NULL)
	{
		printf("----------------\n");
		printf("ATrack\n");
		printf("----------------\n");
		printf("ID: %i\n", a_tracks->i_id);
		printf("Name: %s\n", a_tracks->psz_name);

		a_tracks = a_tracks->p_next;
	}

	// Change log verbosity
	libvlc_set_log_verbosity(instance, LIBVLC_ERROR);



	// Play video
	libvlc_media_player_set_pause(player, 0);

	// Main loop
	while (1)
	{
		// Start timer
		timer = SDL_GetTicks();

		// Check SDL events
		SDL_PollEvent(&event);

		if (event.type == SDL_QUIT)
		{
			break;
		}

		// Check video ending
		if (libvlc_media_player_get_state(player) == libvlc_Ended)
			break;

		// Update the streaming texture
		if (v_update == true)
		{
			SDL_LockTexture(texture, NULL, &v_pixels, &v_pitch);
			memcpy(v_pixels, v_buffer, 800 * 600 * 4);
			SDL_UnlockTexture(texture);
			v_update = false;
		}

		// Render the video
		SDL_SetRenderDrawColor(renderer, 0xff, 0xff, 0xff, 0xff);
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, texture, NULL, NULL);
		SDL_RenderPresent(renderer);

		// Set the real value of ms passed in the main thread
		timer = SDL_GetTicks() - timer;

		// Delay to get fps specified
		SDL_Delay((1000 / fps) - timer);
	}

	// Release VLC resources
	libvlc_media_player_release(player);
	libvlc_release(instance);
	free(v_buffer);

	// Release SDL resources
	SDL_PauseAudioDevice(audio_device, 0);
	SDL_CloseAudioDevice(audio_device);
	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
