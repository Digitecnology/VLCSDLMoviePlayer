TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11

SOURCES += main.cpp \
    vlciso369.cpp

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += libvlc

unix: PKGCONFIG += sdl2

HEADERS += \
    vlciso369.h
