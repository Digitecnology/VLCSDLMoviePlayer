#include "vlciso369.h"
#include <string.h>

const char *FVlcISO639::GetLangDescription(const char *psz_code)
{
	for(auto Element : Languages)
	{
		if(strcmp(Element[2], psz_code) == 0)
			return Element[0];
	}

	return "Undefined";
}

const char *FVlcISO639::GetLang_1(const char *psz_code)
{
	for(auto Element : Languages)
	{
		if(strcmp(Element[2], psz_code) == 0)
			return Element[1];
	}

	return "Undefined";
}

const char *FVlcISO639::GetLang_2T(const char *psz_code)
{
	for(auto Element : Languages)
	{
		if(strcmp(Element[2], psz_code) == 0)
			return Element[2];
	}

	return "Undefined";
}

const char *FVlcISO639::GetLang_2B(const char *psz_code)
{
	for(auto Element : Languages)
	{
		if(strcmp(Element[2], psz_code) == 0)
			return Element[3];
	}

	return "Undefined";
}
